﻿using UnityEngine;
using System.Collections;
using Leap;
using System;

public class Testi : FingerTip
{
	// Leap motion controller attribute
    Leap.Controller controller;

    // Use this for initialization
    void Start()
    {
		// Initialize the leap motion controller for later usage in the code
        controller = new Controller();
		// Basic way to initialize gestures for leap motion. Currently not used in the code
        // controller.EnableGesture(Gesture.GestureType.TYPE_SWIPE);
    }

    // Update is called once per frame
    void Update()
    {
		// Used to detect frames from the movement of hands/fingers
        Frame frame = controller.Frame();
        // Detect swipe gesture
        foreach (Gesture gesture in frame.Gestures())
        {
            switch (gesture.Type)
            {
                case (Gesture.GestureType.TYPE_SWIPE):
                    {
                        Debug.Log("Swipe gesture recognized");
                        linePoints.Clear();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }
    }
}
