﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// When "Space" is pressed, load the level (in other words Scene (in editor)) "test"
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Application.LoadLevel("test");
        }
		// When "Esc" is pressed and user in in the menu-scene, quit the application
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
