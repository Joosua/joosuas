﻿using UnityEngine;
using Leap;
using System.Collections.Generic;


public class FingerTip : MonoBehaviour
{
	// Region can be used to separate parts of the code in to differenct sections. Doesn't affect the code at all, just something to help the development and understanding
    #region Attributes
    // Attribute for Leap motion controller prefab (name of the controller can be found browsing the assets in Unity editor, but the name should always be HandController
    public HandController HandController;
	// List for the points in the line that will be drawn
    public List<Vector3> linePoints;
	// One of Unity features that can be used to draw lines in the game/application
    LineRenderer lineRenderer;
	// Leap motion API, attribute for detecting individual finger/s later in the code
    FingerModel finger;
	
	// Boolean to detect wether the finger is detected or not (in this code, it's also wether the Space is pressed)
    private bool fingerdetect;
	// Float to determine how far apart the coordinates of the linepoints will be registered (something that can be played around with to get the result needed)
    private float newPointDelta = 0.02f;
	// Vector3 (x,y,z -axis) attribute. Used later to detect where the users index fingertip is in the Unitys xyz-axis
    public Vector3 fingerTipPos;
	// Vector3 attribute. Used later to move the camera around
    private Vector3 movement;
	// Float attribute to determine the speed of the camera movement
    float speed = 1.5f;

    #endregion
    // Use this for initialization
    void Start()
    {
        #region Setup Linerenderer
		// Setting up the Linerenderer
		// Find the game object from the Unity
        lineRenderer = gameObject.AddComponent<LineRenderer>();
		// Set the material for it
        lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
        lineRenderer.SetVertexCount(0);
		// Set the width
        lineRenderer.SetWidth(0.1f , 0.1f);
		// Set the colors (start color, end color)
        lineRenderer.SetColors(Color.red, Color.green);
		// Change the Linerenderer to use the real world coordination/axis
        lineRenderer.useWorldSpace = true;
        fingerdetect = false;
        linePoints = new List<Vector3>();
        #endregion
    }

    // Update is called once per frame
    void Update()
    {
        #region Fingerdetect
		// Set the Leap motion to use the graphic models of the hands
        HandModel[] allGraphicHands = HandController.GetAllGraphicsHands();

        if (allGraphicHands.Length <= 0)
        {
            return;
        }

        HandModel handModel = allGraphicHands[0];
		// Single out the index finger
        finger = handModel.fingers[(int)Finger.FingerType.TYPE_INDEX];
		// Get the position of the index fingertip
        fingerTipPos = finger.GetTipPosition();
		// Fingerdetect boolean is given Input attribute (when Space is pressed)
        fingerdetect = (Input.GetKey(KeyCode.Space));

        //Print the position of index fingertip in the coords to Debuglog
        //Debug.Log(fingerTipPos);

        if (fingerdetect)
        {
            Vector3 previousPoint = (linePoints.Count > 0) ? linePoints[linePoints.Count - 1] : new Vector3(-1000, -1000, -1000);

            if (Vector3.Distance(fingerTipPos, previousPoint) > newPointDelta)
            {
                linePoints.Add(fingerTipPos);
                lineRenderer.SetVertexCount(linePoints.Count);
                lineRenderer.SetPosition(linePoints.Count - 1, (Vector3)linePoints[linePoints.Count - 1]);
                //Debug.Log(string.Format("Point added at: {0}!"));
            }
        }
        #region Erase
		// Clears the whole list of linepoints
        else if (Input.GetKey(KeyCode.LeftControl))
        {
            linePoints.Clear();
        }
        #endregion

        #endregion

        #region Camera movement
		// Set the Vector3 to zero before the camera is moved
        movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
        {
            movement.z += 5f;
        }

        if (Input.GetKey(KeyCode.S))
        {
            movement.z -= 5f;
        }

        if (Input.GetKey(KeyCode.A))
        {
            movement.x -= 5f;
        }

        if (Input.GetKey(KeyCode.D))
        {
            movement.x += 5f;
        }

        transform.Translate(movement * Time.deltaTime);

        if (Input.GetKey(KeyCode.Q))
        {
            transform.Rotate(0,-1,0);
        }

        if (Input.GetKey(KeyCode.E))
        {
            transform.Rotate(0, 1, 0);
        }

        //Restrict camera movement
        transform.Translate(movement * speed * Time.deltaTime, Space.Self);
        transform.position = new Vector3(Mathf.Clamp(transform.position.x,
            -20, 20),
            Mathf.Clamp(transform.position.y, -20, 20),
            Mathf.Clamp(transform.position.z, -20, 20));
        #endregion

        #region Change color
        /*if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            lineRenderer.SetColors(Color.green , Color.green);
        }*/
        #endregion

        #region Stop
		// Go back to menu
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel("Main_menu");
        }
        #endregion
    }

}
